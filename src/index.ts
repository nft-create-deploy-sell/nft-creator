import gm from "gm";
import random from "random";
import randomColor from "randomcolor";

const dirName = "D:/images/";
const filesCount = 5;
export default class NFT {
  static async createNFT() {
    let fileId = 1;
    while (true) {
      gm(1000, 1000, "#0b070f").write(
        `${dirName}${fileId}.jpg`,
        function (err) {
          if (err) console.log(err);
        }
      );
      fileId++;
      if (fileId > filesCount) {
        break;
      }
    }
  }
  static async sleep(ms: number) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }

  static async changeNFT() {
    let fileId = 1;
    while (true) {
      let steps = random.int(4, 10);
      console.log(steps);
      let color = randomColor();
      for (let i = 0; i < steps; i++) {
        gm(`${dirName}${fileId}.jpg`)
          .fill(color)
          .drawCircle(
            random.int(100, 900),
            random.int(100, 900),
            random.int(100, 900),
            random.int(100, 900)
          )
          .blur(7, 3)
          .edge(3)
          .write(`${dirName}${fileId}.jpg`, function (err) {
            if (err) console.log(err);
          });
        await NFT.sleep(100);
      }
      fileId++;
      if (fileId > filesCount) {
        break;
      }
    }
  }
}

NFT.createNFT();

NFT.changeNFT();